import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Flight } from './flight.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';

@Entity('price')
export class Price {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Flight, (flight) => flight.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'flight' })
  @Column({ type: 'int' })
  flight?: Flight;

  @Column({ name: 'seat_class', length: 10 })
  seatClass: ESeatClass;

  @Column({ name: 'price_config', type: 'int' })
  priceConfig: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
