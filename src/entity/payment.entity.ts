import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Reservation } from './reservation.entity';
import { PaymentDetail } from './payment-detail.entity';
import { EBankChoice } from 'src/enum/bank-choice.enum';

@Entity('payment')
export class Payment {
  @PrimaryColumn('uuid')
  id: string;

  @ManyToOne(() => Reservation, (reservation) => reservation.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'reservation' })
  @Column({ type: 'int' })
  reservation?: Reservation;

  @Column({ name: 'payment_method', length: '10' })
  paymentMethod: EBankChoice;

  @Column({ name: 'payment_final', type: 'int' })
  paymentFinal: number;

  @Column({ name: 'payment_status', length: '20' })
  paymentStatus: string;

  @Column({ name: 'charge_time', type: 'datetime' })
  chargeTime: string;

  @Column({ name: 'check_time', type: 'datetime', nullable: true })
  checkTime: string;

  @Column({ name: 'payment_time', type: 'datetime', nullable: true })
  paymentTime: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => PaymentDetail, (paymentDetail) => paymentDetail.payment, {
    cascade: ['insert', 'update'],
  })
  details?: PaymentDetail[];
}
