import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Airplane } from './airplane.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';

@Entity('seat')
export class Seat {
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column({ length: 10 })
  code: string;

  @Column({ name: 'seat_class', length: 10 })
  seatClass: ESeatClass;

  @Column({ length: 10 })
  side: ESeatSide;

  @Column({ length: 10 })
  position: ESeatPosition;

  @ManyToOne(() => Airplane, (airplane) => airplane.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'airplane' })
  @Column({ type: 'int' })
  airplane?: Airplane;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
