import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Flight } from './flight.entity';
import { Seat } from './seat.entity';
import { Passenger } from './passenger.entity';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { Payment } from './payment.entity';
import { ReservationJourney } from './reservation-journey.entity';

@Entity('reservation')
export class Reservation {
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column({ type: 'uuid' })
  partner: string;

  @ManyToOne(() => Passenger, (passanger) => passanger.identityNumber, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'passenger' })
  @Column({ length: 16 })
  passenger?: Passenger;

  @Column({ type: 'int', nullable: true })
  member: number;

  @Index()
  @Column({ length: 50 })
  email: string;

  @Index()
  @Column({ length: 20 })
  phone: string;

  @Index()
  @Column({ name: 'flight_date', type: 'date' })
  flightDate: string;

  @ManyToOne(() => Flight, (flight) => flight.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'flight' })
  @Column({ type: 'int' })
  flight?: Flight;

  @ManyToOne(() => Seat, (seat) => seat.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'seat' })
  @Column({ type: 'int' })
  seat?: Seat;

  @Column({ name: 'price_actual', type: 'int' })
  priceActual: number;

  @Index()
  @Column({ name: 'booking_code', length: 10, nullable: true })
  bookingCode: string;

  @Index()
  @Column({ name: 'reservation_code', length: 10, nullable: true })
  reservationCode: string;

  @Index()
  @Column({ name: 'ticket_number', length: 15, nullable: true })
  ticketNumber: string;

  @Column({ name: 'promotion_code', length: 10, nullable: true })
  promotionCode: string;

  @Index()
  @Column({ name: 'current_status', length: 20 })
  currentStatus: EReservationStatus;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => Payment, (payment) => payment.reservation)
  payments?: Payment[];

  @OneToMany(() => ReservationJourney, (journey) => journey.reservation, {
    cascade: ['insert', 'update'],
  })
  journeys?: ReservationJourney[];
}
