import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Airport } from './airport.entity';
import { Airplane } from './airplane.entity';
import { Baggage } from './baggage.entity';
import { Price } from './price.entity';

@Entity('flight')
export class Flight {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 10 })
  code: string;

  @ManyToOne(() => Airport, (departure) => departure.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'departure' })
  @Column({ type: 'int' })
  departure?: Airport;

  @ManyToOne(() => Airport, (destination) => destination.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'destination' })
  @Column({ type: 'int' })
  destination?: Airport;

  @ManyToOne(() => Airplane, (airplane) => airplane.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'airplane' })
  @Column({ type: 'int' })
  airplane?: Airplane;

  @Column({ name: 'departure_time', type: 'time' })
  departureTimeInWIB: string;

  @Column({ name: 'arrival_time', type: 'time' })
  arrivalTimeInWIB: string;

  @Column({ name: 'duration', type: 'int' })
  durationInMinutes: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @ManyToMany(() => Baggage, (baggages) => baggages.flights)
  @JoinTable({
    name: 'rel_flight_baggage',
    joinColumn: {
      name: 'flight',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'baggage',
      referencedColumnName: 'id',
    },
  })
  baggages?: Baggage[];

  @OneToMany(() => Price, (price) => price.flight)
  prices?: Price[];
}
