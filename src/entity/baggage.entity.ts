import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Flight } from './flight.entity';

@Entity('baggage')
export class Baggage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'smallint' })
  capacity: number;

  @Column({ length: 50 })
  category: string;

  @Column({ name: 'price', type: 'int' })
  price: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @ManyToMany(() => Flight, (flights) => flights.baggages)
  flights?: Flight[];
}
