import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('passenger')
export class Passenger {
  @PrimaryColumn({ name: 'identity_number', length: 16 })
  identityNumber: string;

  @Column({ length: 50 })
  name: string;

  @Column({ name: 'birth_date', type: 'date' })
  birthDate: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
