import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Seat } from './seat.entity';

@Entity('airplane')
export class Airplane {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ name: 'registration_number', length: 30 })
  registrationNumber: string;

  @Column({ name: 'max_passenger', type: 'smallint' })
  maxPassenger: number;

  @Column({ name: 'max_business', type: 'smallint' })
  maxBusiness: number;

  @Column({ name: 'max_economy', type: 'smallint' })
  maxEconomy: number;

  @Column({ name: 'chair_config', length: 40 })
  chairConfig: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => Seat, (seat) => seat.airplane)
  seats?: Seat[];
}
