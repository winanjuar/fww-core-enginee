import { ETimezone } from 'src/enum/timezone.enum';

export const getCurrentTimeAdjustedTimezone = (timezone: ETimezone): string => {
  const today = new Date();
  let result: Date;
  if (timezone === ETimezone.WIB) {
    result = new Date(today.getTime() - -420 * 60 * 1000);
  } else if (timezone === ETimezone.WITA) {
    result = new Date(today.getTime() - -480 * 60 * 1000);
  } else {
    result = new Date(today.getTime() - -540 * 60 * 1000);
  }
  return result.toISOString().slice(0, 19).replace('T', ' ');
};

export const getLocaltimeFromUTC = (
  utcString: string,
  timezone: ETimezone,
): string => {
  const utcDate = new Date(utcString);
  let result: Date;
  if (timezone === ETimezone.WIB) {
    result = new Date(utcDate.getTime() - -420 * 60 * 1000);
  } else if (timezone === ETimezone.WITA) {
    result = new Date(utcDate.getTime() - -480 * 60 * 1000);
  } else {
    result = new Date(utcDate.getTime() - -540 * 60 * 1000);
  }
  return result.toISOString().slice(0, 19).replace('T', ' ');
};
