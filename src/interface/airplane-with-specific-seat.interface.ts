import { ISeat } from './seat.interface';

export interface IAirplaneWithSpecificSeat {
  id: number;
  name: string;
  registrationNumber: string;
  maxPassenger: number;
  maxBusiness: number;
  maxEconomy: number;
  chairConfig: string;
  seat: ISeat;
}
