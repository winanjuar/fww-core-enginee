import { IAirplaneWithSpecificSeat } from './airplane-with-specific-seat.interface';

export interface IFlightWithSpecificSeat {
  id: number;
  code: string;
  departure: number;
  destination: number;
  airplane: IAirplaneWithSpecificSeat;
  departureTimeInWIB: string;
  arrivalTimeInWIB: string;
  durationInMinutes: number;
}
