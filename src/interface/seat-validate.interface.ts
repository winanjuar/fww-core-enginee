import { ESeatClass } from 'src/enum/seat-class.enum';

export interface ISeatValidate {
  isValid: boolean;
  flight: number;
  departure: number;
  destination: number;
  airplane: number;
  seat: number;
  seatClass: ESeatClass;
  seatNumber: string;
  price: number;
}
