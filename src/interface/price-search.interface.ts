import { ESeatClass } from 'src/enum/seat-class.enum';

export interface IPriceSearch {
  flight: number;
  seatClass: ESeatClass;
}
