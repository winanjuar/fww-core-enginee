import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { IGenericId } from './generic-id.interface';
import { IPassenger } from './passenger.interface';

export interface IReservationNew {
  partner: string;
  passenger: IPassenger;
  phone: string;
  email: string;
  member?: number;
  flightDate: string;
  flight: IGenericId;
  seat: IGenericId;
  priceActual: number;
  currentStatus: EReservationStatus;
  journeys: IReservationJourney[];
}

export interface IReservationJourney {
  reservation?: IGenericId;
  description: EReservationStatus;
  journeyTime: string;
}
