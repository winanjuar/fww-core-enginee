import { Test, TestingModule } from '@nestjs/testing';
import { faker, fakerID_ID } from '@faker-js/faker';
import {
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ReservationController } from './reservation.controller';
import { ReservationService } from 'src/service/reservation.service';
import { ReservationDto } from 'src/dto/reservation.dto';
import { Reservation } from 'src/entity/reservation.entity';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { ReservationJourney } from 'src/entity/reservation-journey.entity';
import { Passenger } from 'src/entity/passenger.entity';
import { Airport } from 'src/entity/airport.entity';
import { Airplane } from 'src/entity/airplane.entity';
import { Flight } from 'src/entity/flight.entity';
import { Seat } from 'src/entity/seat.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { IReservationUpdateRequest } from 'src/interface/reservation-update.interface';
import { Payment } from 'src/entity/payment.entity';
import { PaymentDetail } from 'src/entity/payment-detail.entity';
import { EBankChoice } from 'src/enum/bank-choice.enum';
import {
  IPaymentDetail,
  IPaymentMaster,
  IPaymentUpdate,
} from 'src/interface/payment.interface';

describe('ReservationController', () => {
  let controller: ReservationController;

  const reservationService = {
    createReservation: jest.fn(),
    getReservation: jest.fn(),
    getReservationByCode: jest.fn(),
    getReservationByTicket: jest.fn(),
    updateReservation: jest.fn(),
    chargePayment: jest.fn(),
    updatePayment: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReservationController],
      providers: [
        { provide: ReservationService, useValue: reservationService },
      ],
    }).compile();
    module.useLogger(false);
    controller = module.get<ReservationController>(ReservationController);
  });

  afterEach(() => jest.clearAllMocks());

  describe('newReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockReservationDto: ReservationDto;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservationDto = {
        partner: faker.string.uuid(),
        ...mockPassenger,
        phone: mockReservation.phone,
        email: mockReservation.email,
        member: mockReservation.member,
        flightDate: mockReservation.flightDate,
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        priceActual: faker.number.int({ min: 500000 }),
        reservationTime: faker.date.recent().toISOString(),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response new reservation just made', async () => {
      // arrange

      const createReservation = jest
        .spyOn(reservationService, 'createReservation')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await controller.newReservation(mockReservationDto);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(createReservation).toHaveBeenCalledTimes(1);
      expect(createReservation).toHaveBeenCalledWith(mockReservationDto);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const createReservation = jest
        .spyOn(reservationService, 'createReservation')
        .mockRejectedValue(
          new UnprocessableEntityException('Failed to save reservation'),
        );

      // act
      const reservation = controller.newReservation(mockReservationDto);

      // assert
      await expect(reservation).rejects.toEqual(
        new UnprocessableEntityException('Failed to save reservation'),
      );
      expect(createReservation).toHaveBeenCalledTimes(1);
      expect(createReservation).toHaveBeenCalledWith(mockReservationDto);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const createReservation = jest
        .spyOn(reservationService, 'createReservation')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const reservation = controller.newReservation(mockReservationDto);

      // assert
      await expect(reservation).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(createReservation).toHaveBeenCalledTimes(1);
      expect(createReservation).toHaveBeenCalledWith(mockReservationDto);
    });
  });

  describe('getReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response data reservation', async () => {
      // arrange
      const id = mockReservation.id;

      const getReservationSpy = jest
        .spyOn(reservationService, 'getReservation')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await controller.getReservation(id);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(getReservationSpy).toHaveBeenCalledTimes(1);
      expect(getReservationSpy).toHaveBeenCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getReservationSpy = jest
        .spyOn(reservationService, 'getReservation')
        .mockRejectedValue(new NotFoundException('Reservation not found'));

      // act
      const reservation = controller.getReservation(id);

      // assert
      await expect(reservation).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(getReservationSpy).toHaveBeenCalledTimes(1);
      expect(getReservationSpy).toHaveBeenCalledWith(id);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });
      const getReservationSpy = jest
        .spyOn(reservationService, 'getReservation')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const reservation = controller.getReservation(id);

      // assert
      await expect(reservation).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getReservationSpy).toHaveBeenCalledTimes(1);
      expect(getReservationSpy).toHaveBeenCalledWith(id);
    });
  });

  describe('getByReservationCode', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response data reservation', async () => {
      // arrange
      mockReservation.reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const identityNumber = mockPassenger.identityNumber;
      const reservationCode = mockReservation.reservationCode;

      const getReservationByCode = jest
        .spyOn(reservationService, 'getReservationByCode')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await controller.getByReservationCode(
        identityNumber,
        reservationCode,
      );

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(getReservationByCode).toHaveBeenCalledTimes(1);
      expect(getReservationByCode).toHaveBeenCalledWith(
        identityNumber,
        reservationCode,
      );
    });

    it('should throw not found exception', async () => {
      // arrange
      mockReservation.reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const identityNumber = mockPassenger.identityNumber;
      const reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const getReservationByCode = jest
        .spyOn(reservationService, 'getReservationByCode')
        .mockRejectedValue(new NotFoundException('Reservation not found'));

      // act
      const getByReservationCode = controller.getByReservationCode(
        identityNumber,
        reservationCode,
      );

      // assert
      await expect(getByReservationCode).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(getReservationByCode).toHaveBeenCalledTimes(1);
      expect(getReservationByCode).toHaveBeenCalledWith(
        identityNumber,
        reservationCode,
      );
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const identityNumber = mockPassenger.identityNumber;
      const reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const getReservationByCode = jest
        .spyOn(reservationService, 'getReservationByCode')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getByReservationCode = controller.getByReservationCode(
        identityNumber,
        reservationCode,
      );

      // assert
      await expect(getByReservationCode).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getReservationByCode).toHaveBeenCalledTimes(1);
      expect(getReservationByCode).toHaveBeenCalledWith(
        identityNumber,
        reservationCode,
      );
    });
  });

  describe('getByTicketNumber', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response data reservation', async () => {
      // arrange
      mockReservation.ticketNumber = faker.string.numeric({ length: 13 });
      const ticketNumber = mockReservation.ticketNumber;

      const getReservationByTicket = jest
        .spyOn(reservationService, 'getReservationByTicket')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await controller.getByTicketNumber(ticketNumber);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(getReservationByTicket).toHaveBeenCalledTimes(1);
      expect(getReservationByTicket).toHaveBeenCalledWith(ticketNumber);
    });

    it('should throw not found exception', async () => {
      // arrange
      mockReservation.ticketNumber = faker.string.numeric({ length: 13 });
      const ticketNumber = faker.string.numeric({ length: 13 });

      const getReservationByTicket = jest
        .spyOn(reservationService, 'getReservationByTicket')
        .mockRejectedValue(new NotFoundException('Reservation not found'));

      // act
      const getByTicketNumber = controller.getByTicketNumber(ticketNumber);

      // assert
      await expect(getByTicketNumber).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(getReservationByTicket).toHaveBeenCalledTimes(1);
      expect(getReservationByTicket).toHaveBeenCalledWith(ticketNumber);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const ticketNumber = faker.string.numeric({ length: 13 });

      const getReservationByTicket = jest
        .spyOn(reservationService, 'getReservationByTicket')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getByTicketNumber = controller.getByTicketNumber(ticketNumber);

      // assert
      await expect(getByTicketNumber).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getReservationByTicket).toHaveBeenCalledTimes(1);
      expect(getReservationByTicket).toHaveBeenCalledWith(ticketNumber);
    });
  });

  describe('handleUpdateReservationSystemTask', () => {
    let mockReservation: Reservation;

    beforeEach(async () => {
      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: null,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should update data reservation', async () => {
      // arrange
      const reservationUpdateRequest: IReservationUpdateRequest = {
        id: mockReservation.id,
        status: EReservationStatus.PAID,
        journeyTime: faker.date.recent().toISOString(),
      };

      mockReservation.currentStatus = reservationUpdateRequest.status;

      const updateReservation = jest
        .spyOn(reservationService, 'updateReservation')
        .mockResolvedValue(mockReservation);

      // act
      const handleUpdateReservationSystemTask =
        controller.handleUpdateReservationSystemTask(reservationUpdateRequest);

      // assert
      await expect(handleUpdateReservationSystemTask).resolves.not.toThrow();
      expect(updateReservation).toHaveBeenCalledTimes(1);
      expect(updateReservation).toHaveBeenCalledWith(reservationUpdateRequest);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const reservationUpdateRequest: IReservationUpdateRequest = {
        id: mockReservation.id,
        status: EReservationStatus.PAID,
        journeyTime: faker.date.recent().toISOString(),
      };

      mockReservation.currentStatus = reservationUpdateRequest.status;

      const updateReservation = jest
        .spyOn(reservationService, 'updateReservation')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleUpdateReservation'),
        );

      // act
      const handleUpdateReservationSystemTask =
        controller.handleUpdateReservationSystemTask(reservationUpdateRequest);

      // assert
      await expect(handleUpdateReservationSystemTask).rejects.toEqual(
        new InternalServerErrorException('Error in handleUpdateReservation'),
      );
      expect(updateReservation).toHaveBeenCalledTimes(1);
      expect(updateReservation).toHaveBeenCalledWith(reservationUpdateRequest);
    });
  });

  describe('handleChargePayment', () => {
    let mockPayment: Payment;
    let mockPaymentDetail: PaymentDetail;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        paymentMethod: faker.helpers.enumValue(EBankChoice),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        details: [mockPaymentDetail],
      };

      mockPaymentDetail = {
        id: faker.string.uuid(),
        payment: mockPayment,
        name: faker.string.sample(),
        quantity: faker.number.int({ min: 1 }),
        price: faker.number.int({ min: 100000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should write new payment', async () => {
      // arrange
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dataPayment: IPaymentMaster = {
        id: mockPayment.id,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };

      const chargePayment = jest
        .spyOn(reservationService, 'chargePayment')
        .mockResolvedValue(mockPayment);

      // act
      const handleUpdateReservationSystemTask =
        controller.handleChargePayment(dataPayment);

      // assert
      await expect(handleUpdateReservationSystemTask).resolves.not.toThrow();
      expect(chargePayment).toHaveBeenCalledTimes(1);
      expect(chargePayment).toHaveBeenCalledWith(dataPayment);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dataPayment: IPaymentMaster = {
        id: mockPayment.id,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };

      delete dataPayment.paymentStatus;

      const chargePayment = jest
        .spyOn(reservationService, 'chargePayment')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleChargePayment'),
        );

      // act
      const handleUpdatePayment = controller.handleChargePayment(dataPayment);

      // assert
      await expect(handleUpdatePayment).rejects.toEqual(
        new InternalServerErrorException('Error in handleChargePayment'),
      );
      expect(chargePayment).toHaveBeenCalledTimes(1);
      expect(chargePayment).toHaveBeenCalledWith(dataPayment);
    });
  });

  describe('handleUpdatePayment', () => {
    let mockPayment: Payment;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        paymentMethod: faker.helpers.enumValue(EBankChoice),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should update data payment', async () => {
      // arrange
      const dataPayment: IPaymentUpdate = {
        id: mockPayment.id,
        paymentStatus: 'settlement',
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
      };

      const updatePayment = jest
        .spyOn(reservationService, 'updatePayment')
        .mockResolvedValue(mockPayment);

      // act
      const handleChargePayment = controller.handleUpdatePayment(dataPayment);

      // assert
      await expect(handleChargePayment).resolves.not.toThrow();
      expect(updatePayment).toHaveBeenCalledTimes(1);
      expect(updatePayment).toHaveBeenCalledWith(dataPayment);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const dataPayment: IPaymentMaster = {
        id: mockPayment.id,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
      };

      delete dataPayment.paymentStatus;

      const updatePayment = jest
        .spyOn(reservationService, 'updatePayment')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleUpdatePayment'),
        );

      // act
      const handleUpdatePayment = controller.handleUpdatePayment(dataPayment);

      // assert
      await expect(handleUpdatePayment).rejects.toEqual(
        new InternalServerErrorException('Error in handleUpdatePayment'),
      );
      expect(updatePayment).toHaveBeenCalledTimes(1);
      expect(updatePayment).toHaveBeenCalledWith(dataPayment);
    });
  });
});
