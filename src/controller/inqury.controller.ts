import {
  Body,
  Controller,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  Post,
  Query,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBasicAuth,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import { FilterFlightDto } from 'src/dto/filter-flight.dto';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { FlightSeatDto } from 'src/dto/flight-seat.dto';
import { InternalServerErrorResponseDto } from 'src/dto/response/internal-server-error.response.dto';
import { ListAirportResponseDto } from 'src/dto/response/list-airport.response.dto';
import { ListFlightResponseDto } from 'src/dto/response/list-flight.response.dto';
import { NotFoundResponseDto } from 'src/dto/response/not-found.response.dto';
import { UnauthorizedResponseDto } from 'src/dto/response/unauthorized.response.dto';
import { UnprocessableEntityResponseDto } from 'src/dto/response/unprocessable-entity.response.dto';
import { SeatClassDto } from 'src/dto/seat-class.dto';
import { InquiryService } from 'src/service/inquiry.service';
@ApiTags('Inquiry')
@ApiInternalServerErrorResponse({ type: InternalServerErrorResponseDto })
@ApiUnauthorizedResponse({ type: UnauthorizedResponseDto })
@ApiBasicAuth()
@Controller({ path: 'inquiry', version: '1' })
export class InquiryController {
  private readonly logger = new Logger(InquiryController.name);
  constructor(private readonly inquiryService: InquiryService) {}

  @ApiNotFoundResponse({ type: NotFoundResponseDto })
  @ApiOkResponse({ type: ListAirportResponseDto })
  @ApiQuery({ type: FilterAirportDto })
  @UseGuards(AuthGuard('basic'))
  @Get('airport')
  async getAirport(@Query() query: FilterAirportDto) {
    this.logger.log('[GET] /api/v1/inquiry/airport');
    try {
      const airports = await this.inquiryService.searchAirport(query);
      this.logger.log(
        `Return data airports with criteria ${JSON.stringify(query)}`,
      );
      return airports;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @ApiNotFoundResponse({ type: NotFoundResponseDto })
  @ApiOkResponse({ type: ListFlightResponseDto })
  @ApiQuery({ type: FilterFlightDto })
  @UseGuards(AuthGuard('basic'))
  @Get('flight')
  async getFlight(@Query() query: FilterFlightDto) {
    this.logger.log('[GET] /api/v1/inquiry/flight');
    try {
      const flights = await this.inquiryService.filterFlight(query);
      this.logger.log(
        `Return data flights with criteria ${JSON.stringify(query)}`,
      );
      return flights;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Post('flight-candidate')
  @HttpCode(200)
  async getFlightCandidate(@Body() flightDto: FlightCandidateDto) {
    this.logger.log('[POST] /api/v1/inquiry/flight-candidate');
    try {
      const flights = await this.inquiryService.searchFlight(flightDto);
      this.logger.log(
        `Return data flight candidates ${JSON.stringify(flightDto)}`,
      );
      return flights;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @ApiUnprocessableEntityResponse({ type: UnprocessableEntityResponseDto })
  @UseGuards(AuthGuard('basic'))
  @Post('seat-validate')
  @HttpCode(200)
  async seatValidate(@Body() flightSeatDto: FlightSeatDto) {
    this.logger.log(`[POST] /api/v1/inquiry/seat-validate`);
    try {
      const validate = await this.inquiryService.seatValidate(flightSeatDto);
      this.logger.log(
        `Return validating result flight seat with with criteria ${JSON.stringify(
          flightSeatDto,
        )}`,
      );
      return validate;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('flight/:id/seat')
  async getFlightSeatsById(
    @Param('id') id: number,
    @Query() query: SeatClassDto,
  ) {
    this.logger.log(`[GET] /api/v1/inquiry/flight/${id}/seat`);
    try {
      const flightSeats = await this.inquiryService.getFlightSeatsById(
        id,
        query,
      );
      this.logger.log(`Return data flight ${id} with seats`);
      return flightSeats;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('flight/:id/baggage')
  async getFlightBaggagesById(@Param('id') id: number) {
    this.logger.log(`[GET] /api/v1/inquiry/flight/${id}/baggage`);
    try {
      const flightBaggages = await this.inquiryService.getFlightBaggagesById(
        id,
      );
      this.logger.log(`Return data flight ${id} with baggages`);
      return flightBaggages;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('flight/:id/airport')
  async getFlightAirportsById(@Param('id') id: number) {
    this.logger.log(`[GET] /api/v1/inquiry/flight/${id}/airport`);
    try {
      const flightAirports = await this.inquiryService.getFlightAirportsById(
        id,
      );
      this.logger.log(`Return data flight ${id} with airports`);
      return flightAirports;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }
}
