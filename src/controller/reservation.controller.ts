import {
  Body,
  Controller,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { EPatternMessage } from 'src/core/pattern-message.enum';
import { ReservationDto } from 'src/dto/reservation.dto';
import {
  IPaymentMaster,
  IPaymentUpdate,
} from 'src/interface/payment.interface';
import { IReservationUpdateRequest } from 'src/interface/reservation-update.interface';
import { ReservationService } from 'src/service/reservation.service';

@ApiTags('Reservation')
@Controller({ path: 'reservation', version: '1' })
export class ReservationController {
  private readonly logger = new Logger(ReservationController.name);
  constructor(private readonly reservationService: ReservationService) {}

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post()
  async newReservation(@Body() reservationDto: ReservationDto) {
    this.logger.log(`[POST] /api/v1/reservation`);
    try {
      const reservation = await this.reservationService.createReservation(
        reservationDto,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Get(':id')
  async getReservation(@Param('id', ParseIntPipe) id: number) {
    this.logger.log(`[POST] /api/v1/reservation/${id}`);
    try {
      const reservation = await this.reservationService.getReservation(id);
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('by-code/:identityNumber/:reservationCode')
  async getByReservationCode(
    @Param('identityNumber') identityNumber: string,
    @Param('reservationCode') reservationCode: string,
  ) {
    this.logger.log(
      `[GET] /api/v1/reservation/by-code/${identityNumber}/${reservationCode}`,
    );
    try {
      const reservation = await this.reservationService.getReservationByCode(
        identityNumber,
        reservationCode,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('by-ticket/:ticketNumber')
  async getByTicketNumber(@Param('ticketNumber') ticketNumber: string) {
    this.logger.log(`[GET] /api/v1/reservation/by-ticket/${ticketNumber}`);
    try {
      const reservation = await this.reservationService.getReservationByTicket(
        ticketNumber,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @EventPattern(EPatternMessage.UPDATE_RESERVATION)
  async handleUpdateReservationSystemTask(@Payload() data: any) {
    this.logger.log(`[EP] ${EPatternMessage.UPDATE_RESERVATION}`);
    try {
      const dataReservation = data as IReservationUpdateRequest;
      await this.reservationService.updateReservation(dataReservation);
      this.logger.log('Update reservation done');
    } catch (error) {
      throw new InternalServerErrorException(
        'Error in handleUpdateReservation',
      );
    }
  }

  @EventPattern(EPatternMessage.CHARGE_PAYMENT)
  async handleChargePayment(@Payload() data: any) {
    this.logger.log(`[EP] ${EPatternMessage.CHARGE_PAYMENT}`);
    try {
      const paymentData = data as IPaymentMaster;
      await this.reservationService.chargePayment(paymentData);
      this.logger.log('Charge payment done');
    } catch (error) {
      throw new InternalServerErrorException('Error in handleChargePayment');
    }
  }

  @EventPattern(EPatternMessage.UPDATE_PAYMENT)
  async handleUpdatePayment(@Payload() data: any) {
    this.logger.log(`[EP] ${EPatternMessage.UPDATE_PAYMENT}`);
    try {
      const paymentData = data as IPaymentUpdate;
      await this.reservationService.updatePayment(paymentData);
      this.logger.log('Update payment done');
    } catch (error) {
      throw new InternalServerErrorException('Error in handleUpdatePayment');
    }
  }
}
