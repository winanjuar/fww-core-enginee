import { Test, TestingModule } from '@nestjs/testing';
import { faker, fakerID_ID } from '@faker-js/faker';
import { InquiryController } from './inqury.controller';
import { InquiryService } from 'src/service/inquiry.service';
import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import { Airport } from 'src/entity/airport.entity';
import {
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Flight } from 'src/entity/flight.entity';
import { Airplane } from 'src/entity/airplane.entity';
import { FilterFlightDto } from 'src/dto/filter-flight.dto';
import { Price } from 'src/entity/price.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { ISeatValidate } from 'src/interface/seat-validate.interface';
import { FlightSeatDto } from 'src/dto/flight-seat.dto';
import { Baggage } from 'src/entity/baggage.entity';
import { Seat } from 'src/entity/seat.entity';
import { SeatClassDto } from 'src/dto/seat-class.dto';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';

describe('InquiryController', () => {
  let controller: InquiryController;

  const inquiryService = {
    searchAirport: jest.fn(),
    filterFlight: jest.fn(),
    searchFlight: jest.fn(),
    getFlightSeatsById: jest.fn(),
    seatValidate: jest.fn(),
    getFlightBaggagesById: jest.fn(),
    getFlightAirportsById: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InquiryController],
      providers: [{ provide: InquiryService, useValue: inquiryService }],
    }).compile();
    module.useLogger(false);
    controller = module.get<InquiryController>(InquiryController);
  });

  afterEach(() => jest.clearAllMocks());

  describe('searchAirport', () => {
    let mockAirport: Airport;
    let mockQueryName: string;
    let mockQueryCity: string;
    let mockQueryCode: string;

    beforeEach(async () => {
      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockQueryName =
        mockAirport.name.length <= 5
          ? mockAirport.name
          : mockAirport.name.substring(
              2,
              Math.floor(Math.random() * (mockAirport.name.length - 3) + 3),
            );

      mockQueryCity =
        mockAirport.city.length <= 5
          ? mockAirport.city
          : mockAirport.city.substring(
              2,
              Math.floor(Math.random() * (mockAirport.city.length - 3) + 3),
            );

      mockQueryCode = mockAirport.code.substring(1);
    });

    afterEach(() => jest.clearAllMocks());

    it('should response list airport without criteria', async () => {
      // arrange
      const query: FilterAirportDto = null;
      const searchAirport = jest
        .spyOn(inquiryService, 'searchAirport')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await controller.getAirport(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(searchAirport).toHaveBeenCalledTimes(1);
      expect(searchAirport).toHaveBeenCalledWith(query);
    });

    it('should response list airport with name, city and code contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: mockQueryName,
        code: mockQueryCode,
        city: mockQueryCity,
      };

      const searchAirport = jest
        .spyOn(inquiryService, 'searchAirport')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await controller.getAirport(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(searchAirport).toHaveBeenCalledTimes(1);
      expect(searchAirport).toHaveBeenCalledWith(query);
      airports.forEach((airport) => expect(airport.name).toContain(query.name));
      airports.forEach((airport) => expect(airport.city).toContain(query.city));
      airports.forEach((airport) => expect(airport.code).toContain(query.code));
    });

    it('should throw not found exception', async () => {
      // arrange
      const differentName = faker.airline.airport().name;
      const differentCity = fakerID_ID.location.city();

      const name =
        differentName.length <= 5
          ? differentName
          : differentName.substring(
              2,
              Math.floor(Math.random() * (differentName.length - 3) + 3),
            );

      const city =
        differentCity.length <= 5
          ? differentCity
          : differentCity.substring(
              2,
              Math.floor(Math.random() * (differentCity.length - 3) + 3),
            );

      const query: FilterAirportDto = {
        name,
        code: null,
        city,
      };

      const searchAirport = jest
        .spyOn(inquiryService, 'searchAirport')
        .mockRejectedValue(new NotFoundException('Airport not found'));

      // act
      const getAirport = controller.getAirport(query);

      // assert
      await expect(getAirport).rejects.toEqual(
        new NotFoundException('Airport not found'),
      );
      expect(searchAirport).toHaveBeenCalledTimes(1);
      expect(searchAirport).toHaveBeenCalledWith(query);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        code: null,
        city: null,
      };

      const searchAirport = jest
        .spyOn(inquiryService, 'searchAirport')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getAirport = controller.getAirport(query);

      // assert
      await expect(getAirport).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(searchAirport).toHaveBeenCalledTimes(1);
      expect(searchAirport).toHaveBeenCalledWith(query);
    });
  });

  describe('getFlight', () => {
    let mockDeparture: Airport;
    let mockDestination: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;

    let mockQueryDeparture: string;
    let mockQueryDestination: string;

    beforeEach(async () => {
      mockDeparture = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockDestination = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockQueryDeparture =
        mockDeparture.name.length <= 5
          ? mockDeparture.name
          : mockDeparture.name.substring(
              2,
              Math.floor(Math.random() * (mockDeparture.name.length - 3) + 3),
            );

      mockQueryDestination =
        mockDestination.city.length <= 5
          ? mockDestination.city
          : mockDestination.city.substring(
              2,
              Math.floor(Math.random() * (mockDestination.city.length - 3) + 3),
            );
    });

    afterEach(() => jest.clearAllMocks());

    it('should response list flight without criteria', async () => {
      // arrange
      const query: FilterFlightDto = null;
      const filterFlight = jest
        .spyOn(inquiryService, 'filterFlight')
        .mockResolvedValue([mockFlight]);

      // act
      const flights = await controller.getFlight(query);

      // assert
      expect(flights).toEqual([mockFlight]);
      expect(filterFlight).toHaveBeenCalledTimes(1);
      expect(filterFlight).toHaveBeenCalledWith(query);
    });

    it('should response list flight with departure and destination contains criteria value', async () => {
      // arrange
      const query: FilterFlightDto = {
        departure: mockQueryDeparture,
        destination: mockQueryDestination,
      };
      const filterFlight = jest
        .spyOn(inquiryService, 'filterFlight')
        .mockResolvedValue([mockFlight]);

      // act
      const flights = await controller.getFlight(query);

      // assert
      expect(flights).toEqual([mockFlight]);
      expect(filterFlight).toHaveBeenCalledTimes(1);
      expect(filterFlight).toHaveBeenCalledWith(query);
      flights.forEach((flight) =>
        expect(
          flight.departure.name.includes(query.departure) ||
            flight.departure.city.includes(query.departure) ||
            flight.departure.code.includes(query.departure),
        ).toBe(true),
      );

      flights.forEach((flight) =>
        expect(
          flight.destination.name.includes(query.destination) ||
            flight.destination.city.includes(query.destination) ||
            flight.destination.code.includes(query.destination),
        ).toBe(true),
      );
    });

    it('should throw not found exception', async () => {
      // arrange
      const departure = faker.string.sample();
      const destination = faker.string.sample();

      const query: FilterFlightDto = {
        departure,
        destination,
      };

      const filterFlight = jest
        .spyOn(inquiryService, 'filterFlight')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const getFlight = controller.getFlight(query);

      // assert
      await expect(getFlight).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(filterFlight).toHaveBeenCalledTimes(1);
      expect(filterFlight).toHaveBeenCalledWith(query);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const departure = faker.string.sample();
      const destination = faker.string.sample();

      const query: FilterFlightDto = {
        departure,
        destination,
      };

      const filterFlight = jest
        .spyOn(inquiryService, 'filterFlight')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getFlight = controller.getFlight(query);

      // assert
      await expect(getFlight).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(filterFlight).toHaveBeenCalledTimes(1);
      expect(filterFlight).toHaveBeenCalledWith(query);
    });
  });

  describe('getFlightCandidate', () => {
    let mockDeparture: Airport;
    let mockDestination: Airport;
    let mockAirplane: Airplane;
    let mockPrice: Price;
    let mockFlight: Flight;

    let mockFlightDto: FlightCandidateDto;

    beforeEach(async () => {
      mockDeparture = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockDestination = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPrice = {
        id: faker.number.int({ min: 1 }),
        flight: mockFlight,
        seatClass: faker.helpers.enumValue(ESeatClass),
        priceConfig: faker.number.int({ min: 500000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        prices: [mockPrice],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlightDto = {
        departureId: mockDeparture.id,
        destinationId: mockDestination.id,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response list candidate flight', async () => {
      // arrange
      const searchFlight = jest
        .spyOn(inquiryService, 'searchFlight')
        .mockResolvedValue([mockFlight]);

      // act
      const flights = await controller.getFlightCandidate(mockFlightDto);

      // assert
      expect(flights).toEqual([mockFlight]);
      expect(searchFlight).toHaveBeenCalledTimes(1);
      expect(searchFlight).toHaveBeenCalledWith(mockFlightDto);
      flights.forEach((flight) => {
        expect(flight.departure.id).toEqual(mockFlightDto.departureId);
        expect(flight.destination.id).toEqual(mockFlightDto.destinationId);
      });
    });

    it('should throw not found exception', async () => {
      // arrange
      mockFlightDto = {
        departureId: faker.number.int({ min: 0 }),
        destinationId: faker.number.int({ min: 0 }),
      };

      const searchFlight = jest
        .spyOn(inquiryService, 'searchFlight')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const getFlightCandidate = controller.getFlightCandidate(mockFlightDto);

      // assert
      await expect(getFlightCandidate).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(searchFlight).toHaveBeenCalledTimes(1);
      expect(searchFlight).toHaveBeenCalledWith(mockFlightDto);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      mockFlightDto = {
        departureId: faker.number.int({ min: 0 }),
        destinationId: faker.number.int({ min: 0 }),
      };

      const searchFlight = jest
        .spyOn(inquiryService, 'searchFlight')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getFlightCandidate = controller.getFlightCandidate(mockFlightDto);

      // assert
      await expect(getFlightCandidate).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(searchFlight).toHaveBeenCalledTimes(1);
      expect(searchFlight).toHaveBeenCalledWith(mockFlightDto);
    });
  });

  describe('seatValidate', () => {
    let mockSeatValidate: ISeatValidate;
    let mockFlightSeatDto: FlightSeatDto;

    beforeEach(async () => {
      mockSeatValidate = {
        isValid: true,
        flight: faker.number.int({ min: 1 }),
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatNumber: faker.airline.seat(),
        price: faker.number.int({ min: 500000 }),
      };

      mockFlightSeatDto = {
        flight: mockSeatValidate.flight,
        seat: mockSeatValidate.seat,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response success validation result', async () => {
      // arrange
      const seatValidate = jest
        .spyOn(inquiryService, 'seatValidate')
        .mockResolvedValue(mockSeatValidate);

      // act
      const validate = await controller.seatValidate(mockFlightSeatDto);

      // assert
      expect(validate).toEqual(mockSeatValidate);
      expect(seatValidate).toHaveBeenCalledTimes(1);
      expect(seatValidate).toHaveBeenCalledWith(mockFlightSeatDto);
      expect(validate.flight).toEqual(mockFlightSeatDto.flight);
      expect(validate.seat).toEqual(mockFlightSeatDto.seat);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      mockFlightSeatDto = {
        flight: mockSeatValidate.flight,
        seat: faker.number.int({ min: 1 }),
      };

      const seatValidateSpy = jest
        .spyOn(inquiryService, 'seatValidate')
        .mockRejectedValue(
          new UnprocessableEntityException('Invalid input seat info'),
        );

      // act
      const seatValidate = controller.seatValidate(mockFlightSeatDto);

      // assert
      await expect(seatValidate).rejects.toEqual(
        new UnprocessableEntityException('Invalid input seat info'),
      );
      expect(seatValidateSpy).toHaveBeenCalledTimes(1);
      expect(seatValidateSpy).toHaveBeenCalledWith(mockFlightSeatDto);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const seatValidateSpy = jest
        .spyOn(inquiryService, 'seatValidate')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const seatValidate = controller.seatValidate(mockFlightSeatDto);

      // assert
      await expect(seatValidate).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(seatValidateSpy).toHaveBeenCalledTimes(1);
      expect(seatValidateSpy).toHaveBeenCalledWith(mockFlightSeatDto);
    });
  });

  describe('getFlightSeatsById', () => {
    let mockFlight: Flight;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockSeat: Seat;
    let mockQuerySeatClass: SeatClassDto;

    beforeEach(async () => {
      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        seats: [mockSeat],
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response a flight with seat info both economy and bussiness', async () => {
      // arrange
      const id = mockFlight.id;
      mockQuerySeatClass = {
        seatClass: null,
      };

      const getFlightSeatsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightSeatsById')
        .mockResolvedValue(mockFlight);

      // act
      const flightSeats = await controller.getFlightSeatsById(
        id,
        mockQuerySeatClass,
      );

      // assert
      expect(flightSeats).toEqual(mockFlight);
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledWith(
        id,
        mockQuerySeatClass,
      );
      expect(flightSeats.id).toEqual(id);
    });

    it('should response a flight with seat info with criteria seat class', async () => {
      // arrange
      const id = mockFlight.id;
      mockQuerySeatClass = {
        seatClass: ESeatClass.ECONOMY,
      };

      mockSeat.seatClass = ESeatClass.ECONOMY;

      const getFlightSeatsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightSeatsById')
        .mockResolvedValue(mockFlight);

      // act
      const flightSeats = await controller.getFlightSeatsById(
        id,
        mockQuerySeatClass,
      );

      // assert
      expect(flightSeats).toEqual(mockFlight);
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledWith(
        id,
        mockQuerySeatClass,
      );
      expect(flightSeats.id).toEqual(id);
      flightSeats.airplane.seats.forEach((seat) =>
        expect(seat.seatClass).toEqual(mockQuerySeatClass.seatClass),
      );
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getFlightSeatsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightSeatsById')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const getFlightSeatsById = controller.getFlightSeatsById(
        id,
        mockQuerySeatClass,
      );

      // assert
      await expect(getFlightSeatsById).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledWith(
        id,
        mockQuerySeatClass,
      );
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getFlightSeatsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightSeatsById')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getFlightSeatsById = controller.getFlightSeatsById(
        id,
        mockQuerySeatClass,
      );

      // assert
      await expect(getFlightSeatsById).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightSeatsByIdSpy).toHaveBeenCalledWith(
        id,
        mockQuerySeatClass,
      );
    });
  });

  describe('getFlightBaggagesById', () => {
    let mockFlight: Flight;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockBaggage: Baggage;

    beforeEach(async () => {
      mockBaggage = {
        id: faker.number.int({ min: 1 }),
        capacity: faker.number.int({ min: 1 }),
        category: faker.helpers.arrayElement([
          'Cabin',
          'Normal',
          'Over',
          'Extra Over',
        ]),
        price: faker.number.int({ min: 0 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        baggages: [mockBaggage],
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response a flight with baggage info', async () => {
      // arrange
      const id = mockFlight.id;
      const getFlightBaggagesByIdSpy = jest
        .spyOn(inquiryService, 'getFlightBaggagesById')
        .mockResolvedValue(mockFlight);

      // act
      const flightBaggages = await controller.getFlightBaggagesById(id);

      // assert
      expect(flightBaggages).toEqual(mockFlight);
      expect(getFlightBaggagesByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightBaggagesByIdSpy).toHaveBeenCalledWith(id);
      expect(flightBaggages.id).toEqual(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getFlightBaggagesByIdSpy = jest
        .spyOn(inquiryService, 'getFlightBaggagesById')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const getFlightBaggagesById = controller.getFlightBaggagesById(id);

      // assert
      await expect(getFlightBaggagesById).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(getFlightBaggagesByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightBaggagesByIdSpy).toHaveBeenCalledWith(id);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getFlightBaggagesByIdSpy = jest
        .spyOn(inquiryService, 'getFlightBaggagesById')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getFlightBaggagesById = controller.getFlightBaggagesById(id);

      // assert
      await expect(getFlightBaggagesById).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getFlightBaggagesByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightBaggagesByIdSpy).toHaveBeenCalledWith(id);
    });
  });

  describe('getFlightAirportsById', () => {
    let mockFlight: Flight;
    let mockAirport: Airport;
    let mockAirplane: Airplane;

    beforeEach(async () => {
      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response a flight with airport info', async () => {
      // arrange
      const id = mockFlight.id;
      const getFlightAirportsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightAirportsById')
        .mockResolvedValue(mockFlight);

      // act
      const flightAirports = await controller.getFlightAirportsById(id);

      // assert
      expect(flightAirports).toEqual(mockFlight);
      expect(getFlightAirportsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightAirportsByIdSpy).toHaveBeenCalledWith(id);
      expect(flightAirports.id).toEqual(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getFlightAirportsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightAirportsById')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const getFlightAirportsById = controller.getFlightAirportsById(id);

      // assert
      await expect(getFlightAirportsById).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(getFlightAirportsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightAirportsByIdSpy).toHaveBeenCalledWith(id);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const getFlightAirportsByIdSpy = jest
        .spyOn(inquiryService, 'getFlightAirportsById')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const getFlightAirportsById = controller.getFlightAirportsById(id);

      // assert
      await expect(getFlightAirportsById).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getFlightAirportsByIdSpy).toHaveBeenCalledTimes(1);
      expect(getFlightAirportsByIdSpy).toHaveBeenCalledWith(id);
    });
  });
});
