import { Test, TestingModule } from '@nestjs/testing';
import { faker, fakerID_ID } from '@faker-js/faker';

import { ReservationService } from './reservation.service';
import { ReservationRepository } from 'src/repository/reservation.repository';
import { PassengerRepository } from 'src/repository/passenger.repository';
import { Reservation } from 'src/entity/reservation.entity';
import { ReservationJourney } from 'src/entity/reservation-journey.entity';
import { Passenger } from 'src/entity/passenger.entity';
import { ReservationDto } from 'src/dto/reservation.dto';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import {
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Airport } from 'src/entity/airport.entity';
import { Airplane } from 'src/entity/airplane.entity';
import { Flight } from 'src/entity/flight.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { Seat } from 'src/entity/seat.entity';
import {
  IReservationUpdateData,
  IReservationUpdateRequest,
} from 'src/interface/reservation-update.interface';
import { IReservationJourney } from 'src/interface/reservation-new.interface';
import { ReservationJourneyRepository } from 'src/repository/reservation-journey.repository';
import { PaymentRepository } from 'src/repository/payment.repository';
import { Payment } from 'src/entity/payment.entity';
import { PaymentDetail } from 'src/entity/payment-detail.entity';
import { EBankChoice } from 'src/enum/bank-choice.enum';
import {
  IPaymentDetail,
  IPaymentMaster,
  IPaymentUpdate,
} from 'src/interface/payment.interface';

describe('ReservationService', () => {
  let service: ReservationService;

  const reservationRepo = {
    saveReservation: jest.fn(),
    updateReservation: jest.fn(),
    findReservationDetailById: jest.fn(),
    findReservationByCode: jest.fn(),
    findReservationByTicket: jest.fn(),
  };

  const passengerRepo = {
    savePassenger: jest.fn(),
  };

  const reservationJourneyRepo = {
    write: jest.fn(),
  };

  const paymentRepo = {
    writeNew: jest.fn(),
    updateStatus: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReservationService,
        { provide: PassengerRepository, useValue: passengerRepo },
        { provide: PaymentRepository, useValue: paymentRepo },
        { provide: ReservationRepository, useValue: reservationRepo },
        {
          provide: ReservationJourneyRepository,
          useValue: reservationJourneyRepo,
        },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<ReservationService>(ReservationService);
  });

  afterEach(() => jest.clearAllMocks());

  describe('createReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockReservationDto: ReservationDto;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservationDto = {
        partner: faker.string.uuid(),
        ...mockPassenger,
        phone: mockReservation.phone,
        email: mockReservation.email,
        member: mockReservation.member,
        flightDate: mockReservation.flightDate,
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        priceActual: faker.number.int({ min: 500000 }),
        reservationTime: faker.date.recent().toISOString(),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return new reservation just made', async () => {
      // arrange
      const savePassenger = jest
        .spyOn(passengerRepo, 'savePassenger')
        .mockResolvedValue(mockPassenger);

      const saveReservation = jest
        .spyOn(reservationRepo, 'saveReservation')
        .mockResolvedValue(mockReservation);

      // act
      const newReservation = await service.createReservation(
        mockReservationDto,
      );

      // assert
      expect(newReservation).toEqual(mockReservation);
      expect(savePassenger).toHaveBeenCalledTimes(1);
      expect(saveReservation).toHaveBeenCalledTimes(1);
    });

    it('should throw unprocessable entity exception when insert/update passenger failed', async () => {
      // arrange
      const savePassenger = jest
        .spyOn(passengerRepo, 'savePassenger')
        .mockResolvedValue(null);

      const saveReservation = jest
        .spyOn(reservationRepo, 'saveReservation')
        .mockResolvedValue(mockPassenger);

      // act
      const createReservation = service.createReservation(mockReservationDto);

      // assert
      await expect(createReservation).rejects.toEqual(
        new UnprocessableEntityException('Failed to save reservation'),
      );
      expect(savePassenger).toHaveBeenCalledTimes(1);
      expect(saveReservation).toHaveBeenCalledTimes(1);
    });

    it('should throw unprocessable entity exception when insert reservation failed', async () => {
      // arrange
      const savePassenger = jest
        .spyOn(passengerRepo, 'savePassenger')
        .mockResolvedValue(null);

      const saveReservation = jest
        .spyOn(reservationRepo, 'saveReservation')
        .mockResolvedValue(mockReservation);

      // act
      const createReservation = service.createReservation(mockReservationDto);

      // assert
      await expect(createReservation).rejects.toEqual(
        new UnprocessableEntityException('Failed to save reservation'),
      );
      expect(savePassenger).toHaveBeenCalledTimes(1);
      expect(saveReservation).toHaveBeenCalledTimes(1);
    });

    it('should throw unprocessable entity exception when both insert/update passenger and insert reservation failed', async () => {
      // arrange
      const savePassenger = jest
        .spyOn(passengerRepo, 'savePassenger')
        .mockResolvedValue(null);

      const saveReservation = jest
        .spyOn(reservationRepo, 'saveReservation')
        .mockResolvedValue(null);

      // act
      const createReservation = service.createReservation(mockReservationDto);

      // assert
      await expect(createReservation).rejects.toEqual(
        new UnprocessableEntityException('Failed to save reservation'),
      );
      expect(savePassenger).toHaveBeenCalledTimes(1);
      expect(saveReservation).toHaveBeenCalledTimes(1);
    });
  });

  describe('updateReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;

    beforeEach(async () => {
      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: null,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return reservation just updated', async () => {
      // arrange
      const reservationUpdateRequest: IReservationUpdateRequest = {
        id: mockReservation.id,
        status: EReservationStatus.SUCCESSFUL_BOOKED,
        journeyTime: faker.date.recent().toISOString(),
      };

      const reservationUpdateData: IReservationUpdateData = {
        currentStatus: reservationUpdateRequest.status,
      };

      const reservationJourneyData: IReservationJourney = {
        reservation: { id: reservationUpdateRequest.id },
        description: reservationUpdateRequest.status,
        journeyTime: reservationUpdateRequest.journeyTime,
      };

      mockReservation.currentStatus = reservationUpdateData.currentStatus;

      const updateReservation = jest
        .spyOn(reservationRepo, 'updateReservation')
        .mockResolvedValue(mockReservation);

      const writeSpy = jest
        .spyOn(reservationJourneyRepo, 'write')
        .mockResolvedValue(mockReservationJourney);

      // act
      const reservationUpdated = await service.updateReservation(
        reservationUpdateRequest,
      );

      // assert
      expect(reservationUpdated).toEqual(mockReservation);
      expect(updateReservation).toHaveBeenCalledTimes(1);
      expect(updateReservation).toHaveBeenCalledWith(
        reservationUpdateRequest.id,
        reservationUpdateData,
      );
      expect(writeSpy).toHaveBeenCalledTimes(1);
      expect(writeSpy).toHaveBeenCalledWith(reservationJourneyData);
    });
  });

  describe('getReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation', async () => {
      // arrange
      const id = mockReservation.id;

      const findReservationDetailById = jest
        .spyOn(reservationRepo, 'findReservationDetailById')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await service.getReservation(id);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findReservationDetailById).toHaveBeenCalledTimes(1);
      expect(findReservationDetailById).toHaveBeenCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const findReservationDetailById = jest
        .spyOn(reservationRepo, 'findReservationDetailById')
        .mockResolvedValue(null);

      // act
      const getReservation = service.getReservation(id);

      // assert
      await expect(getReservation).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(findReservationDetailById).toHaveBeenCalledTimes(1);
      expect(findReservationDetailById).toHaveBeenCalledWith(id);
    });
  });

  describe('getReservationByCode', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation', async () => {
      // arrange
      mockReservation.reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const identityNumber = mockPassenger.identityNumber;
      const reservationCode = mockReservation.reservationCode;

      const findReservationByCode = jest
        .spyOn(reservationRepo, 'findReservationByCode')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await service.getReservationByCode(
        identityNumber,
        reservationCode,
      );

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findReservationByCode).toHaveBeenCalledTimes(1);
      expect(findReservationByCode).toHaveBeenCalledWith(
        identityNumber,
        reservationCode,
      );
    });

    it('should throw not found exception', async () => {
      // arrange
      mockReservation.reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const identityNumber = mockPassenger.identityNumber;
      const reservationCode = faker.string.alphanumeric({
        length: 8,
        casing: 'upper',
      });

      const findReservationByCode = jest
        .spyOn(reservationRepo, 'findReservationByCode')
        .mockResolvedValue(null);

      // act
      const getReservationByCode = service.getReservationByCode(
        identityNumber,
        reservationCode,
      );

      // assert
      await expect(getReservationByCode).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(findReservationByCode).toHaveBeenCalledTimes(1);
      expect(findReservationByCode).toHaveBeenCalledWith(
        identityNumber,
        reservationCode,
      );
    });
  });

  describe('getReservationByTicket', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation', async () => {
      // arrange
      mockReservation.ticketNumber = faker.string.numeric({ length: 13 });
      const ticketNumber = mockReservation.ticketNumber;

      const findReservationByTicket = jest
        .spyOn(reservationRepo, 'findReservationByTicket')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await service.getReservationByTicket(ticketNumber);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findReservationByTicket).toHaveBeenCalledTimes(1);
      expect(findReservationByTicket).toHaveBeenCalledWith(ticketNumber);
    });

    it('should throw not found exception', async () => {
      // arrange
      mockReservation.ticketNumber = faker.string.numeric({ length: 13 });
      const ticketNumber = faker.string.numeric({ length: 13 });

      const findReservationByTicket = jest
        .spyOn(reservationRepo, 'findReservationByTicket')
        .mockResolvedValue(null);

      // act
      const getReservationByTicket =
        service.getReservationByTicket(ticketNumber);

      // assert
      await expect(getReservationByTicket).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(findReservationByTicket).toHaveBeenCalledTimes(1);
      expect(findReservationByTicket).toHaveBeenCalledWith(ticketNumber);
    });
  });

  describe('chargePayment', () => {
    let mockPayment: Payment;
    let mockPaymentDetail: PaymentDetail;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        paymentMethod: faker.helpers.enumValue(EBankChoice),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        details: [mockPaymentDetail],
      };

      mockPaymentDetail = {
        id: faker.string.uuid(),
        payment: mockPayment,
        name: faker.string.sample(),
        quantity: faker.number.int({ min: 1 }),
        price: faker.number.int({ min: 100000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data new payment', async () => {
      // arrange
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dataPayment: IPaymentMaster = {
        id: mockPayment.id,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };

      const writeNew = jest
        .spyOn(paymentRepo, 'writeNew')
        .mockResolvedValue(mockPayment);

      // act
      const payment = await service.chargePayment(dataPayment);

      // assert
      expect(payment).toEqual(mockPayment);
      expect(writeNew).toHaveBeenCalledTimes(1);
      expect(writeNew).toHaveBeenCalledWith(dataPayment);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dataPayment: IPaymentMaster = {
        id: mockPayment.id,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };

      const writeNew = jest
        .spyOn(paymentRepo, 'writeNew')
        .mockResolvedValue(null);

      // act
      const chargePayment = service.chargePayment(dataPayment);

      // assert
      await expect(chargePayment).rejects.toEqual(
        new UnprocessableEntityException('Failed write payment'),
      );
      expect(writeNew).toHaveBeenCalledTimes(1);
      expect(writeNew).toHaveBeenCalledWith(dataPayment);
    });
  });

  describe('updatePayment', () => {
    let mockPayment: Payment;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        paymentMethod: faker.helpers.enumValue(EBankChoice),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data updated payment', async () => {
      // arrange
      const dataPayment: IPaymentUpdate = {
        id: mockPayment.id,
        paymentStatus: 'settlement',
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
      };

      const updateStatus = jest
        .spyOn(paymentRepo, 'updateStatus')
        .mockResolvedValue(mockPayment);

      // act
      const payment = await service.updatePayment(dataPayment);

      // assert
      expect(payment).toEqual(mockPayment);
      expect(updateStatus).toHaveBeenCalledTimes(1);
      expect(updateStatus).toHaveBeenCalledWith(dataPayment);
    });
  });
});
