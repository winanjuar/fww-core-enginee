import {
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ReservationDto } from 'src/dto/reservation.dto';

import { Reservation } from 'src/entity/reservation.entity';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import {
  IReservationNew,
  IReservationJourney,
} from 'src/interface/reservation-new.interface';
import { IPassenger } from 'src/interface/passenger.interface';
import { PassengerRepository } from 'src/repository/passenger.repository';

import { ReservationRepository } from 'src/repository/reservation.repository';
import {
  IReservationUpdateData,
  IReservationUpdateRequest,
} from 'src/interface/reservation-update.interface';
import { ReservationJourneyRepository } from 'src/repository/reservation-journey.repository';
import {
  IPaymentMaster,
  IPaymentUpdate,
} from 'src/interface/payment.interface';
import { Payment } from 'src/entity/payment.entity';
import { PaymentRepository } from 'src/repository/payment.repository';

@Injectable()
export class ReservationService {
  private readonly logger = new Logger(ReservationService.name);
  constructor(
    private readonly reservationRepo: ReservationRepository,
    private readonly reservationJourneyRepo: ReservationJourneyRepository,
    private readonly passengerRepo: PassengerRepository,
    private readonly paymentRepo: PaymentRepository,
  ) {}

  async createReservation(
    reservationDto: ReservationDto,
  ): Promise<Reservation> {
    const dataPassenger: IPassenger = {
      identityNumber: reservationDto.identityNumber,
      name: reservationDto.name,
      birthDate: reservationDto.birthDate,
    };

    const dataJourney: IReservationJourney = {
      description: EReservationStatus.NEW,
      journeyTime: reservationDto.reservationTime,
    };

    const dataReservation: IReservationNew = {
      partner: reservationDto.partner,
      passenger: { identityNumber: reservationDto.identityNumber },
      member: reservationDto.member,
      phone: reservationDto.phone,
      email: reservationDto.email,
      flightDate: reservationDto.flightDate,
      flight: { id: reservationDto.flight },
      seat: { id: reservationDto.seat },
      priceActual: reservationDto.priceActual,
      currentStatus: EReservationStatus.NEW,
      journeys: [dataJourney],
    };

    const passenger = await this.passengerRepo.savePassenger(dataPassenger);
    const newReservation = await this.reservationRepo.saveReservation(
      dataReservation,
    );

    if (passenger && newReservation) {
      this.logger.log('Process new reservation');
      return newReservation;
    } else {
      throw new UnprocessableEntityException('Failed to save reservation');
    }
  }

  async updateReservation(
    reservationUpdateRequest: IReservationUpdateRequest,
  ): Promise<Reservation> {
    const reservationUpdateData: IReservationUpdateData = {
      currentStatus: reservationUpdateRequest.status,
      bookingCode: reservationUpdateRequest.bookingCode,
      reservationCode: reservationUpdateRequest.reservationCode,
      ticketNumber: reservationUpdateRequest.ticketNumber,
    };

    const reservationJourneyData: IReservationJourney = {
      reservation: { id: reservationUpdateRequest.id },
      description: reservationUpdateRequest.status,
      journeyTime: reservationUpdateRequest.journeyTime,
    };

    const result = await Promise.all([
      await this.reservationRepo.updateReservation(
        reservationUpdateRequest.id,
        reservationUpdateData,
      ),
      await this.reservationJourneyRepo.write(reservationJourneyData),
    ]);

    const reservation = result[0];

    this.logger.log('Process update reservation');
    return reservation;
  }

  async getReservation(id: number): Promise<Reservation> {
    const reservation = await this.reservationRepo.findReservationDetailById(
      id,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    this.logger.log('Process data reservation');
    return reservation;
  }

  async getReservationByCode(
    identityNumber: string,
    reservationCode: string,
  ): Promise<Reservation> {
    const reservation = await this.reservationRepo.findReservationByCode(
      identityNumber,
      reservationCode,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    this.logger.log('Process data reservation');
    return reservation;
  }

  async getReservationByTicket(ticketNumber: string): Promise<Reservation> {
    const reservation = await this.reservationRepo.findReservationByTicket(
      ticketNumber,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    this.logger.log('Process data reservation');
    return reservation;
  }

  async chargePayment(dataPayment: IPaymentMaster): Promise<Payment> {
    const newPayment = await this.paymentRepo.writeNew(dataPayment);
    if (!newPayment) {
      throw new UnprocessableEntityException('Failed write payment');
    }

    this.logger.log('Process charge payment');
    return newPayment;
  }

  async updatePayment(dataPayment: IPaymentUpdate): Promise<Payment> {
    const payment = await this.paymentRepo.updateStatus(dataPayment);
    this.logger.log('Process update payment');
    return payment;
  }
}
