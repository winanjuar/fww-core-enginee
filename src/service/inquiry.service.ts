import {
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import { FilterFlightDto } from 'src/dto/filter-flight.dto';

import { AirportRepository } from 'src/repository/airport.repository';
import { FlightRepository } from 'src/repository/flight.repository';

import { Airport } from 'src/entity/airport.entity';
import { Flight } from 'src/entity/flight.entity';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { SeatClassDto } from 'src/dto/seat-class.dto';
import { ISeatValidate } from 'src/interface/seat-validate.interface';
import { PriceRepository } from 'src/repository/price.repository';
import { IPriceSearch } from 'src/interface/price-search.interface';
import { SeatRepository } from 'src/repository/seat.repository';
import { FlightSeatDto } from 'src/dto/flight-seat.dto';
import { ISeat } from 'src/interface/seat.interface';
import { IAirplaneWithSpecificSeat } from 'src/interface/airplane-with-specific-seat.interface';
import { IFlightWithSpecificSeat } from 'src/interface/flight-with-specific-seat.interface';

@Injectable()
export class InquiryService {
  private readonly logger = new Logger(InquiryService.name);
  constructor(
    private readonly airportRepo: AirportRepository,
    private readonly flightRepo: FlightRepository,
    private readonly priceRepo: PriceRepository,
    private readonly seatRepo: SeatRepository,
  ) {}

  async searchAirport(query: FilterAirportDto): Promise<Airport[]> {
    const airports = await this.airportRepo.findWithQuery(query);

    if (airports.length === 0) {
      throw new NotFoundException('Airport not found');
    }

    this.logger.log('Process data airports');
    return airports;
  }

  async filterFlight(query: FilterFlightDto): Promise<Flight[]> {
    const flights = await this.flightRepo.findWithQuery(query);

    if (flights.length === 0) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flights');
    return flights;
  }

  async searchFlight(flightDto: FlightCandidateDto): Promise<Flight[]> {
    const flights = await this.flightRepo.findCandidate(flightDto);
    if (flights.length === 0) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight candidates');
    return flights;
  }

  async seatValidate(flightSeatDto: FlightSeatDto): Promise<ISeatValidate> {
    const seat = await this.seatRepo.findOneBySeatId(flightSeatDto.seat);
    const priceCriteria: IPriceSearch = {
      flight: flightSeatDto.flight,
      seatClass: seat.seatClass,
    };

    const resultFlightAndPrice = await Promise.all([
      this.flightRepo.findByIdIncludeSpecificSeat(flightSeatDto),
      this.priceRepo.findByFlightAndSeatClass(priceCriteria),
    ]);

    const flight = resultFlightAndPrice[0];
    const price = resultFlightAndPrice[1];

    if (!flight) {
      throw new UnprocessableEntityException('Invalid input seat info');
    }

    const specificSeat: ISeat = {
      id: flight.airplane.seats[0].id,
      code: flight.airplane.seats[0].code,
      seatClass: flight.airplane.seats[0].seatClass,
      side: flight.airplane.seats[0].side,
      position: flight.airplane.seats[0].position,
    };

    const airplane: IAirplaneWithSpecificSeat = {
      id: flight.airplane.id,
      name: flight.airplane.name,
      registrationNumber: flight.airplane.registrationNumber,
      maxPassenger: flight.airplane.maxPassenger,
      maxBusiness: flight.airplane.maxBusiness,
      maxEconomy: flight.airplane.maxEconomy,
      chairConfig: flight.airplane.chairConfig,
      seat: specificSeat,
    };

    const flightWithSpecificSeat: IFlightWithSpecificSeat = {
      id: flight.id,
      code: flight.code,
      departure: Number(flight.departure),
      destination: Number(flight.destination),
      airplane: airplane,
      departureTimeInWIB: flight.departureTimeInWIB,
      arrivalTimeInWIB: flight.arrivalTimeInWIB,
      durationInMinutes: flight.durationInMinutes,
    };

    this.logger.log('Process data flight include specific seat');
    return {
      isValid: true,
      flight: flightSeatDto.flight,
      departure: flightWithSpecificSeat.departure,
      destination: flightWithSpecificSeat.destination,
      airplane: flightWithSpecificSeat.airplane.id,
      seat: flightSeatDto.seat,
      seatClass: seat.seatClass,
      seatNumber: seat.code,
      price: price.priceConfig,
    } as unknown as ISeatValidate;
  }

  async getFlightBaggagesById(id: number): Promise<Flight> {
    const flight = await this.flightRepo.findByIdIncludeBaggages(id);

    if (!flight) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight include baggages');
    return flight;
  }

  async getFlightSeatsById(id: number, query: SeatClassDto): Promise<Flight> {
    const flight = await this.flightRepo.findByIdIncludeSeats(id, query);

    if (!flight) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight include seats');
    return flight;
  }

  async getFlightAirportsById(id: number): Promise<Flight> {
    const flight = await this.flightRepo.findByIdIncludeAirports(id);

    if (!flight) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight include airports');
    return flight;
  }
}
