import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker, fakerID_ID } from '@faker-js/faker';
import { ReservationRepository } from './reservation.repository';
import { Reservation } from 'src/entity/reservation.entity';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { ReservationJourney } from 'src/entity/reservation-journey.entity';
import {
  IReservationJourney,
  IReservationNew,
} from 'src/interface/reservation-new.interface';
import { IGenericId } from 'src/interface/generic-id.interface';
import { NotFoundException } from '@nestjs/common';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { Passenger } from 'src/entity/passenger.entity';
import { Airport } from 'src/entity/airport.entity';
import { Airplane } from 'src/entity/airplane.entity';
import { Flight } from 'src/entity/flight.entity';
import { Seat } from 'src/entity/seat.entity';
import { IPassenger } from 'src/interface/passenger.interface';
import { IReservationUpdateData } from 'src/interface/reservation-update.interface';

describe('ReservationRepository', () => {
  let repository: ReservationRepository;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReservationRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<ReservationRepository>(ReservationRepository);
  });

  afterEach(() => jest.clearAllMocks());

  describe('findReservationById', () => {
    let mockReservation: Reservation;

    beforeEach(async () => {
      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: faker.helpers.enumValue(EReservationStatus),
        bookingCode: faker.string.alphanumeric({ length: 8, casing: 'upper' }),
        reservationCode: faker.string.alphanumeric({
          length: 6,
          casing: 'upper',
        }),
        ticketNumber: faker.string.numeric({ length: 13 }),
        promotionCode: faker.string.alphanumeric({
          length: 4,
          casing: 'upper',
        }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation', async () => {
      // arrange
      const id = mockReservation.id;

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.findReservationById(id);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(null);

      // act
      const reservation = await repository.findReservationById(id);

      // assert
      expect(reservation).toEqual(null);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });
  });

  describe('saveReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        reservation: mockReservation,
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: faker.helpers.enumValue(EReservationStatus),
        bookingCode: faker.string.alphanumeric({ length: 8, casing: 'upper' }),
        reservationCode: faker.string.alphanumeric({
          length: 6,
          casing: 'upper',
        }),
        ticketNumber: faker.string.numeric({ length: 13 }),
        promotionCode: faker.string.alphanumeric({
          length: 4,
          casing: 'upper',
        }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        journeys: [mockReservationJourney],
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data new reservation with its journey just made', async () => {
      // arrange
      const dataPassenger: IPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
      };

      const genericId: IGenericId = {
        id: faker.number.int({ min: 1 }),
      };

      const dataReservationJourney: IReservationJourney = {
        description: mockReservationJourney.description,
        journeyTime: mockReservationJourney.journeyTime,
      };

      const dataReservation: IReservationNew = {
        partner: faker.string.uuid(),
        passenger: dataPassenger,
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: genericId,
        seat: genericId,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [dataReservationJourney],
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.saveReservation(dataReservation);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(dataReservation);
    });
  });
  describe('updateReservation', () => {
    let mockReservation: Reservation;

    beforeEach(async () => {
      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: null,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation just updated', async () => {
      // arrange
      const id = mockReservation.id;

      const reservationUpdate: IReservationUpdateData = {
        currentStatus: EReservationStatus.PAYMENT_CHARGED,
      };

      const findReservationById = jest
        .spyOn(repository, 'findReservationById')
        .mockResolvedValue(mockReservation);

      mockReservation.currentStatus = reservationUpdate.currentStatus;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.updateReservation(
        id,
        reservationUpdate,
      );

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findReservationById).toBeCalledTimes(1);
      expect(findReservationById).toHaveBeenCalledWith(id);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(mockReservation);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const reservationUpdate: IReservationUpdateData = {
        currentStatus: EReservationStatus.PAYMENT_CHARGED,
      };

      const findReservationById = jest
        .spyOn(repository, 'findReservationById')
        .mockResolvedValue(null);

      const saveSpy = jest.spyOn(repository, 'save').mockResolvedValue(null);

      // act
      const updateReservation = repository.updateReservation(
        id,
        reservationUpdate,
      );

      // assert
      await expect(updateReservation).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(findReservationById).toBeCalledTimes(1);
      expect(findReservationById).toHaveBeenCalledWith(id);
      expect(saveSpy).toBeCalledTimes(0);
    });
  });

  describe('findReservationDetailById', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data detail reservation', async () => {
      // arrange
      const id = mockReservation.id;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.findReservationDetailById(id);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { id },
        relations: {
          flight: { departure: true, destination: true },
          passenger: true,
          seat: { airplane: true },
        },
      });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const reservation = await repository.findReservationDetailById(id);

      // assert
      expect(reservation).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { id },
        relations: {
          flight: { departure: true, destination: true },
          passenger: true,
          seat: { airplane: true },
        },
      });
    });
  });

  describe('findReservationByTicket', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.number.int({ min: 1 }),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.location.timeZone(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
        airplane: mockAirplane,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.number.int({ min: 1 }),
        partner: faker.string.uuid(),
        passenger: mockPassenger,
        member: faker.number.int({ min: 1 }),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        ticketNumber: null,
        promotionCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation', async () => {
      // arrange
      mockReservation.ticketNumber = faker.string.numeric({ length: 13 });
      const ticketNumber = mockReservation.ticketNumber;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.findReservationByTicket(
        ticketNumber,
      );

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { ticketNumber },
      });
    });

    it('should return null', async () => {
      // arrange
      mockReservation.ticketNumber = faker.string.numeric({ length: 13 });
      const ticketNumber = faker.string.numeric({ length: 13 });

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const reservation = await repository.findReservationByTicket(
        ticketNumber,
      );

      // assert
      expect(reservation).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { ticketNumber },
      });
    });
  });
});
