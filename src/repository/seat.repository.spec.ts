import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { SeatRepository } from './seat.repository';
import { Seat } from 'src/entity/seat.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';

describe('SeatRepository', () => {
  let repository: SeatRepository;
  let mockSeat: Seat;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SeatRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<SeatRepository>(SeatRepository);

    mockSeat = {
      id: faker.number.int({ min: 1 }),
      code: faker.airline.seat(),
      seatClass: faker.helpers.enumValue(ESeatClass),
      side: faker.helpers.enumValue(ESeatSide),
      position: faker.helpers.enumValue(ESeatPosition),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('findOneBySeatId', () => {
    it('should return data seat', async () => {
      // arrange
      const id = mockSeat.id;

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(mockSeat);

      // act
      const seat = await repository.findOneBySeatId(id);

      // assert
      expect(seat).toEqual(mockSeat);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(null);

      // act
      const seat = await repository.findOneBySeatId(id);

      // assert
      expect(seat).toEqual(null);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });
  });
});
