import { Injectable, Logger } from '@nestjs/common';
import { DataSource, Like, Repository } from 'typeorm';

import { Airport } from '../entity/airport.entity';

import { FilterAirportDto } from 'src/dto/filter-airport.dto';

@Injectable()
export class AirportRepository extends Repository<Airport> {
  private readonly logger = new Logger(AirportRepository.name);
  constructor(dataSource: DataSource) {
    super(Airport, dataSource.createEntityManager());
  }

  async findWithQuery(query: FilterAirportDto): Promise<Airport[]> {
    const whereCondition = {};
    if (query.city) {
      whereCondition['city'] = Like('%' + query.city + '%');
    }

    if (query.name) {
      whereCondition['name'] = Like('%' + query.name + '%');
    }

    if (query.code) {
      whereCondition['code'] = Like('%' + query.code + '%');
    }

    const airports = await this.find({ where: whereCondition });

    this.logger.log('Query data airports with criteria');
    return airports;
  }
}
