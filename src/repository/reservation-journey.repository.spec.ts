import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { ReservationJourneyRepository } from './reservation-journey.repository';
import { ReservationJourney } from 'src/entity/reservation-journey.entity';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { Reservation } from 'src/entity/reservation.entity';
import { IReservationJourney } from 'src/interface/reservation-new.interface';

describe('ReservationJourneyRepository', () => {
  let repository: ReservationJourneyRepository;
  let mockReservation: Reservation;
  let mockReservationJourney: ReservationJourney;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReservationJourneyRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<ReservationJourneyRepository>(
      ReservationJourneyRepository,
    );

    mockReservation = {
      id: faker.number.int({ min: 1 }),
      partner: faker.string.uuid(),
      member: faker.number.int({ min: 1 }),
      phone: faker.phone.number(),
      email: faker.internet.email(),
      flightDate: faker.date.future().toISOString().split('T')[0],
      flight: null,
      seat: null,
      priceActual: faker.number.int({ min: 500000 }),
      currentStatus: EReservationStatus.NEW,
      journeys: [mockReservationJourney],
      bookingCode: null,
      reservationCode: null,
      ticketNumber: null,
      promotionCode: null,
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockReservationJourney = {
      id: faker.number.int({ min: 1 }),
      description: EReservationStatus.NEW,
      journeyTime: faker.date.recent().toISOString(),
      reservation: mockReservation,
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('write', () => {
    it('should return data reservation journey just made', async () => {
      // arrange
      const reservationJourneyData: IReservationJourney = {
        reservation: { id: mockReservation.id },
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockReservationJourney);

      // act
      const newReservationJourney = await repository.write(
        reservationJourneyData,
      );

      // assert
      expect(newReservationJourney).toEqual(mockReservationJourney);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(reservationJourneyData);
    });
  });
});
