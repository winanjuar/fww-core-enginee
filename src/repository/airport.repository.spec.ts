import { Test, TestingModule } from '@nestjs/testing';
import { DataSource, Like } from 'typeorm';
import { faker, fakerID_ID } from '@faker-js/faker';
import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import { AirportRepository } from './airport.repository';
import { Airport } from '../../src/entity/airport.entity';

describe('AirportRepository', () => {
  let repository: AirportRepository;
  let mockAirport: Airport;
  let mockQueryName: string;
  let mockQueryCity: string;
  let mockQueryCode: string;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AirportRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<AirportRepository>(AirportRepository);

    mockAirport = {
      id: faker.number.int({ min: 1 }),
      code: faker.airline.airport().iataCode,
      name: faker.airline.airport().name,
      city: fakerID_ID.location.city(),
      timezone: faker.location.timeZone(),
      createdAt: faker.date.anytime().toISOString(),
      updatedAt: faker.date.anytime().toISOString(),
      deletedAt: null,
    };

    mockQueryName =
      mockAirport.name.length <= 5
        ? mockAirport.name
        : mockAirport.name.substring(
            2,
            Math.floor(Math.random() * (mockAirport.name.length - 3) + 3),
          );

    mockQueryCity =
      mockAirport.city.length <= 5
        ? mockAirport.city
        : mockAirport.city.substring(
            2,
            Math.floor(Math.random() * (mockAirport.city.length - 3) + 3),
          );

    mockQueryCode = mockAirport.code.substring(1);
  });

  afterEach(() => jest.clearAllMocks());

  describe('findWithQuery', () => {
    it('should return list airport without criteria', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        code: null,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({ where: {} });
    });

    it('should return list airport with name contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: mockQueryName,
        code: null,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { name: Like('%' + mockQueryName + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.name).toContain(mockQueryName);
      });
    });

    it('should return list airport with code contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        code: mockQueryCode,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { code: Like('%' + mockQueryCode + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.code).toContain(mockQueryCode);
      });
    });

    it('should return list airport with city contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        city: mockQueryCity,
        code: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { city: Like('%' + mockQueryCity + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.city).toContain(mockQueryCity);
      });
    });

    it('should return list airport with name, code and city contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: mockQueryName,
        city: mockQueryCity,
        code: mockQueryCode,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: {
          name: Like('%' + mockQueryName + '%'),
          city: Like('%' + mockQueryCity + '%'),
          code: Like('%' + mockQueryCode + '%'),
        },
      });
      airports.forEach((airport) => {
        expect(airport.name).toContain(mockQueryName);
      });
      airports.forEach((airport) => {
        expect(airport.city).toContain(mockQueryCity);
      });
      airports.forEach((airport) => {
        expect(airport.code).toContain(mockQueryCode);
      });
    });

    it('should return empty array', async () => {
      // arrange
      const differentName = faker.airline.airport().name;
      const differentCity = fakerID_ID.location.city();

      const name =
        differentName.length <= 5
          ? differentName
          : differentName.substring(
              2,
              Math.floor(Math.random() * (differentName.length - 3) + 3),
            );

      const city =
        differentCity.length <= 5
          ? differentCity
          : differentCity.substring(
              2,
              Math.floor(Math.random() * (differentCity.length - 3) + 3),
            );

      const query: FilterAirportDto = {
        city,
        name,
        code: null,
      };

      const findSpy = jest.spyOn(repository, 'find').mockResolvedValue([]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { city: Like('%' + city + '%'), name: Like('%' + name + '%') },
      });
    });
  });
});
