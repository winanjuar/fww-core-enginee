import { Injectable, Logger } from '@nestjs/common';
import {
  Brackets,
  DataSource,
  Repository,
  WhereExpressionBuilder,
} from 'typeorm';

import { Flight } from 'src/entity/flight.entity';
import { FilterFlightDto } from 'src/dto/filter-flight.dto';
import { SeatClassDto } from 'src/dto/seat-class.dto';
import { FlightSeatDto } from 'src/dto/flight-seat.dto';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';

@Injectable()
export class FlightRepository extends Repository<Flight> {
  private readonly logger = new Logger(FlightRepository.name);
  constructor(dataSource: DataSource) {
    super(Flight, dataSource.createEntityManager());
  }

  async findByIdIncludeBaggages(id: number): Promise<Flight> {
    const flight = await this.findOne({
      relations: ['baggages'],
      where: { id },
    });
    this.logger.log('Query data flight include baggages with criteria');
    return flight;
  }

  async findByIdIncludeAirports(id: number): Promise<Flight> {
    const flight = await this.findOne({
      relations: ['departure', 'destination'],
      where: { id },
    });
    this.logger.log('Query data flight include airports with criteria');
    return flight;
  }

  async findByIdIncludeSeats(id: number, query: SeatClassDto): Promise<Flight> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('airplane.seats', 'seat')
      .where('flight.id = :id', { id });

    if (query.seatClass) {
      flightQB.andWhere('seat.seatClass = :seatClass', {
        seatClass: query.seatClass,
      });
    }

    const flight = await flightQB.getOne();
    this.logger.log('Query data flight include seats with criteria');
    return flight;
  }

  async findByIdIncludeSpecificSeat(
    flightSeatDto: FlightSeatDto,
  ): Promise<Flight> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('airplane.seats', 'seat')
      .where('flight.id = :flightId', { flightId: flightSeatDto.flight })
      .andWhere('seat.id = :seatId', {
        seatId: flightSeatDto.seat,
      });
    const flight = await flightQB.getOne();

    this.logger.log('Query data flight include specific seat');
    return flight;
  }

  async findWithQuery(query: FilterFlightDto): Promise<Flight[]> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.departure', 'departure')
      .innerJoinAndSelect('flight.destination', 'destination')
      .innerJoinAndSelect('flight.airplane', 'airplane');

    if (query.departure) {
      flightQB.andWhere(
        new Brackets((departureWEB: WhereExpressionBuilder) => {
          departureWEB
            .where('departure.code LIKE :departure', {
              departure: `%${query.departure}%`,
            })
            .orWhere('departure.name LIKE :departure', {
              departure: `%${query.departure}%`,
            })
            .orWhere('departure.city LIKE :departure', {
              departure: `%${query.departure}%`,
            });
        }),
      );
    }

    if (query.destination) {
      flightQB.andWhere(
        new Brackets((destinationWEB: WhereExpressionBuilder) => {
          destinationWEB
            .where('destination.code LIKE :destination', {
              destination: `%${query.destination}%`,
            })
            .orWhere('destination.name LIKE :destination', {
              destination: `%${query.destination}%`,
            })
            .orWhere('destination.city LIKE :destination', {
              destination: `%${query.destination}%`,
            });
        }),
      );
    }
    const flights = await flightQB.getMany();
    this.logger.log('Query data flights with criteria');
    return flights;
  }

  async findCandidate(criteria: FlightCandidateDto): Promise<Flight[]> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.departure', 'departure')
      .innerJoinAndSelect('flight.destination', 'destination')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('flight.prices', 'price')
      .where('flight.departure = :departure', {
        departure: criteria.departureId,
      })
      .andWhere('flight.destination = :destination', {
        destination: criteria.destinationId,
      });

    const flights = await flightQB.getMany();

    this.logger.log('Query data flight candidates with criteria');
    return flights;
  }
}
