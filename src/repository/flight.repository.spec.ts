import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker, fakerID_ID } from '@faker-js/faker';
import { FlightRepository } from './flight.repository';
import { Flight } from 'src/entity/flight.entity';
import { Airport } from 'src/entity/airport.entity';
import { Airplane } from 'src/entity/airplane.entity';
import { Baggage } from 'src/entity/baggage.entity';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { Seat } from 'src/entity/seat.entity';

describe('FlightRepository', () => {
  let repository: FlightRepository;
  let mockFlight: Flight;
  let mockAirport: Airport;
  let mockAirplane: Airplane;
  let mockBaggage: Baggage;
  let mockSeat: Seat;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FlightRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<FlightRepository>(FlightRepository);

    mockBaggage = {
      id: faker.number.int({ min: 1 }),
      capacity: faker.number.int({ min: 1 }),
      category: faker.helpers.arrayElement([
        'Cabin',
        'Normal',
        'Over',
        'Extra Over',
      ]),
      price: faker.number.int({ min: 0 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockSeat = {
      id: faker.number.int({ min: 1 }),
      code: faker.airline.seat(),
      seatClass: faker.helpers.enumValue(ESeatClass),
      side: faker.helpers.enumValue(ESeatSide),
      position: faker.helpers.enumValue(ESeatPosition),
      airplane: mockAirplane,
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockAirplane = {
      id: faker.number.int({ min: 1 }),
      name: faker.airline.airplane().name,
      registrationNumber: faker.airline.airplane().iataTypeCode,
      maxPassenger: faker.number.int({ min: 60, max: 172 }),
      maxBusiness: faker.number.int({ min: 0 }),
      maxEconomy: faker.number.int({ min: 50 }),
      chairConfig: faker.string.sample(),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      seats: [mockSeat],
    };

    mockAirport = {
      id: faker.number.int({ min: 1 }),
      code: faker.airline.airport().iataCode,
      name: faker.airline.airport().name,
      city: fakerID_ID.location.city(),
      timezone: faker.location.timeZone(),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockFlight = {
      id: faker.number.int({ min: 1 }),
      code: faker.airline.flightNumber(),
      departure: mockAirport,
      destination: mockAirport,
      airplane: mockAirplane,
      departureTimeInWIB: faker.date.anytime().toTimeString(),
      arrivalTimeInWIB: faker.date.anytime().toTimeString(),
      durationInMinutes: faker.number.int({ min: 60 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      baggages: [mockBaggage],
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('findByIdIncludeBaggages', () => {
    it('should return query data flight include baggages', async () => {
      // arrange
      const id = mockFlight.id;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await repository.findByIdIncludeBaggages(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['baggages'],
        where: { id },
      });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const flight = await repository.findByIdIncludeBaggages(id);

      // assert
      expect(flight).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['baggages'],
        where: { id },
      });
    });
  });

  describe('findByIdIncludeAirports', () => {
    it('should return query data flight include airport', async () => {
      // arrange
      delete mockFlight.baggages;

      const id = mockFlight.id;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await repository.findByIdIncludeAirports(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['departure', 'destination'],
        where: { id },
      });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const flight = await repository.findByIdIncludeAirports(id);

      // assert
      expect(flight).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['departure', 'destination'],
        where: { id },
      });
    });
  });
});
