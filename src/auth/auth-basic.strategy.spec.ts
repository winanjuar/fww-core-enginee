import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { UnauthorizedException } from '@nestjs/common';
import { AuthBasicStrategy } from './auth-basic.strategy';

describe('AuthBasicStrategy', () => {
  let app: TestingModule;
  let strategy: AuthBasicStrategy;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [
        AuthBasicStrategy,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              if (key === 'BASIC_USER') {
                return 'valid_user';
              } else if (key === 'BASIC_PASS') {
                return 'valid_password';
              }
              return null;
            }),
          },
        },
      ],
    }).compile();

    strategy = app.get<AuthBasicStrategy>(AuthBasicStrategy);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should validate the correct username and password', async () => {
    // arrange
    const username = 'valid_user';
    const password = 'valid_password';

    // act
    const result = await strategy.validate(null, username, password);

    // assert
    expect(result).toBe(true);
  });

  it('should throw unauthorized exception for invalid username or password', async () => {
    // arrange
    const username = 'invalid_user';
    const password = 'invalid_password';

    // act
    const validate = strategy.validate(null, username, password);

    // assert
    await expect(validate).rejects.toThrow(new UnauthorizedException());
  });

  afterAll(async () => {
    await app.close();
  });
});
