export enum EPatternMessage {
  UPDATE_RESERVATION = 'EP_UpdateReservation',
  CHARGE_PAYMENT = 'EP_ChargePayment',
  UPDATE_PAYMENT = 'EP_UpdatePayment',
}
