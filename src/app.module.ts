import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';

import { InquiryController } from './controller/inqury.controller';
import { ReservationController } from './controller/reservation.controller';

import { InquiryService } from './service/inquiry.service';
import { ReservationService } from './service/reservation.service';

import { AirportRepository } from './repository/airport.repository';
import { FlightRepository } from './repository/flight.repository';
import { ReservationRepository } from './repository/reservation.repository';

import { Airport } from './entity/airport.entity';
import { Airplane } from './entity/airplane.entity';
import { Seat } from './entity/seat.entity';
import { Flight } from './entity/flight.entity';
import { Baggage } from './entity/baggage.entity';
import { Passenger } from './entity/passenger.entity';
import { Reservation } from './entity/reservation.entity';
import { Price } from './entity/price.entity';
import { PassengerRepository } from './repository/passenger.repository';
import { PriceRepository } from './repository/price.repository';
import { SeatRepository } from './repository/seat.repository';
import { Payment } from './entity/payment.entity';
import { PaymentDetail } from './entity/payment-detail.entity';
import { ReservationJourney } from './entity/reservation-journey.entity';
import { ReservationJourneyRepository } from './repository/reservation-journey.repository';
import { PaymentRepository } from './repository/payment.repository';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mariadb',
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USER'),
        password: configService.get<string>('DB_PASS'),
        database: configService.get<string>('DB_NAME'),
        entities: [
          Airport,
          Airplane,
          Seat,
          Flight,
          Price,
          Baggage,
          Passenger,
          Reservation,
          ReservationJourney,
          Payment,
          PaymentDetail,
        ],
        synchronize: configService.get<boolean>('DB_SYNC') || false,
        dateStrings: true,
      }),
    }),
    AuthModule,
  ],
  controllers: [InquiryController, ReservationController],
  providers: [
    InquiryService,
    ReservationService,
    AirportRepository,
    FlightRepository,
    PassengerRepository,
    ReservationRepository,
    ReservationJourneyRepository,
    PaymentRepository,
    PriceRepository,
    SeatRepository,
  ],
})
export class AppModule {}
