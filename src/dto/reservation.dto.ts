import {
  IsDateString,
  IsEmail,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
  Length,
} from 'class-validator';

export class ReservationDto {
  @IsUUID()
  @IsNotEmpty()
  partner: string;

  @IsString()
  @Length(16)
  @IsNotEmpty()
  identityNumber: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsISO8601({ strict: true })
  @IsDateString()
  @IsNotEmpty()
  birthDate: string;

  @IsPhoneNumber()
  @IsNotEmpty()
  phone: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsOptional()
  @IsNumber()
  member: number;

  @IsISO8601({ strict: true })
  @IsDateString()
  @IsNotEmpty()
  flightDate: string;

  @IsNumber()
  @IsNotEmpty()
  flight: number;

  @IsNumber()
  @IsNotEmpty()
  seat: number;

  @IsNumber()
  @IsNotEmpty()
  priceActual: number;

  @IsDateString()
  @IsNotEmpty()
  reservationTime: string;
}
