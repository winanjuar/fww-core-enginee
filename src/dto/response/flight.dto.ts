import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { AirportDto } from './airport.dto';
import { AirplaneDto } from './airplane.dto';

export class FlightDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  departure: AirportDto;

  @ApiProperty()
  @IsNotEmpty()
  destination: AirportDto;

  @ApiProperty()
  @IsNotEmpty()
  airplane: AirplaneDto;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  departureTimeInWIB: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  arrivalTimeInWIB: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  duration: number;
}
