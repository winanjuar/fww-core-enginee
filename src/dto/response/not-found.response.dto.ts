import { ApiProperty } from '@nestjs/swagger';

export class NotFoundResponseDto {
  @ApiProperty({ example: 404 })
  statusCode: number;

  @ApiProperty({
    example: `This is sample message not found`,
  })
  message: string;

  @ApiProperty({ example: 'Not Found' })
  error: string;
}
