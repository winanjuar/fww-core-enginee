import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class AirplaneDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  registrationNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  maxPassenger: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  maxBusiness: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  maxEconomy: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  chairConfig: string;
}
