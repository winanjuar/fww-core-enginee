import { ApiProperty } from '@nestjs/swagger';

export class UnauthorizedResponseDto {
  @ApiProperty({ example: 401 })
  statusCode: number;

  @ApiProperty({ example: 'This is sample message unauthorized' })
  message: string;

  @ApiProperty({ example: 'Unauthorized' })
  error: string;
}
