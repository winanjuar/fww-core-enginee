import { ApiProperty } from '@nestjs/swagger';

export class UnprocessableEntityResponseDto {
  @ApiProperty({ example: 422 })
  statusCode: number;

  @ApiProperty({
    example: `This is sample unprocessable entity`,
  })
  message: string;

  @ApiProperty({ example: 'Unprocessable Entity' })
  error: string;
}
