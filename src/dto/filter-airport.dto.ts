import { IsOptional, IsString } from 'class-validator';

export class FilterAirportDto {
  @IsString()
  @IsOptional()
  city: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  code: string;
}
