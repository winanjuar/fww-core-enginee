import { IsNotEmpty, IsNumber } from 'class-validator';

export class FlightCandidateDto {
  @IsNumber()
  @IsNotEmpty()
  departureId: number;

  @IsNumber()
  @IsNotEmpty()
  destinationId: number;
}
