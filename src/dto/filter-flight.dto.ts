import { IsOptional, IsString } from 'class-validator';

export class FilterFlightDto {
  @IsString()
  @IsOptional()
  departure: string;

  @IsString()
  @IsOptional()
  destination: string;
}
