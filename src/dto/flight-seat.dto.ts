import { IsNotEmpty, IsNumber } from 'class-validator';

export class FlightSeatDto {
  @IsNumber()
  @IsNotEmpty()
  flight: number;

  @IsNumber()
  @IsNotEmpty()
  seat: number;
}
